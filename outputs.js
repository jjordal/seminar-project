
var form = document.getElementById('form');

form.addEventListener('submit', function(event){
    event.preventDefault();
    let weight = document.getElementById('weight').value;
    let activity = document.getElementById('activity').value;
    let sex = document.getElementById('sex').value;
    let height_ft = document.getElementById('height_ft').value;
    let height_in = document.getElementById('height_in').value;
    let age = document.getElementById('age').value;
    let goal = document.getElementById('goal').value;
    goal = goal * 1;
    let height_cm = ((height_ft * 12) + (height_in * 1)) * 2.54;
    let BMR = 0;
    if(sex == "M"){
        BMR = (10 * (weight / 2.2)) + (6.25 * height_cm) - (5 * age) + 5;
    }else{
        BMR = (10 * (weight / 2.2)) + (6.25 * height_cm) - (5 * age) - 161;
    }
    
    if(activity == "1"){
        BMR = BMR * 1.375;
    }else if(activity == "2"){
        BMR = BMR * 1.55;
    }else if(activity == "3"){
        BMR = BMR * 1.725;
    }else if(activity == "4"){
        BMR = BMR * 1.9;
    }else{
        BMR = BMR * 1.2;
    }

    BMR += goal * 500;
    BMR = Math.round(BMR);
    let fats =  (BMR * 0.25);
    let carbs = (BMR - fats) - (weight / 4);

    var result = document.getElementById('result');
    result.innerHTML = BMR + " calories";

    var result = document.getElementById('protien');
    result.innerHTML = weight + " grams";

    var result = document.getElementById('carbs');
    result.innerHTML = Math.round(carbs/4) + " grams";

    var result = document.getElementById('fat');
    result.innerHTML = Math.round(fats / 9) + " grams";

})

var form = document.getElementById('form_max');

form.addEventListener('submit', function(event){
    event.preventDefault();
    let weight = document.getElementById('weight_max').value;
    let reps = document.getElementById('reps').value;
    weight = weight * 1;
    reps = reps * 1;
    let max = weight / (1.0278 - (0.0278 * reps));
    var result = document.getElementById('result_max');
    result.innerHTML = Math.round(max);

})

var form = document.getElementById('form_track');

form.addEventListener('submit', function(event){
    event.preventDefault();
    let cals = document.getElementById('calories').value;
    let change;
    let change_type = document.getElementById('type').value;
    let weeks = document.getElementById('weeks').value;
    let goal = document.getElementById('goal_track').value;

    var ele = document.getElementsByName('change');
              
        for(i = 0; i < ele.length; i++) {
            if(ele[i].checked)
                change = ele[i].value;
        }

    change = change * 1;
    change_type = change_type * 1;
    change = change * change_type;
    cals = cals*1;
    goal = goal *1;
    weeks = weeks * 1;

    var result = document.getElementById('result_track');

    change = change * -1;
    change = goal + change;
    change = (change * 500) / weeks;
    cals = cals + change;

    

    
    result.innerHTML = Math.round(cals);

})
